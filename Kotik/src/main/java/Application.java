import model.Kotik;

public class Application {
    public static void main(String[] args) {

        Kotik kat = new Kotik(12, "Kote", 25, "myu myu");
        kat.setNumEat(11);
        kat.setFoodName("wiskas");
        String katMeow = kat.getMeow();
        kat.liveAnotherDay();

        Kotik cat = new Kotik();
        cat.setNumEat(11);
        cat.setFoodName("мясо");
        cat.setKotik(111, "Фунтик", 22, "gav gav gav");
        String catMeow = cat.getMeow();
        String name = cat.getName();
        int getWeight = cat.getWeight();
        cat.liveAnotherDay();

        System.out.println("Имя и рост котика: " + name + " " + getWeight);
        System.out.println("Результат сравнения переменных " + catMeow.equals(katMeow));
        System.out.println("Создано объектов: " + Kotik.getObjectCount());

    }
}
