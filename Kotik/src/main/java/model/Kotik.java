package model;

public class Kotik {

    private static int objectCount;

    private int fullBelly;
    private int prettiness;
    private int weight;
    private String foodName;
    private String name;
    private String meow;

    public Kotik() {
        objectCount++;
    }

    public Kotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        objectCount++;
    }

    public void setKotik(int prettiness, String name, int weight, String meow) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public void setNumEat(int fullBelly) {
        this.fullBelly = fullBelly;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public int getNumEat() {
        return fullBelly;
    }

    public String getFoodName() {
        return foodName;
    }

    public String getMeow() {
        return meow;
    }

    public static int getObjectCount() {
        return objectCount;
    }

    public void liveAnotherDay() {
        for (int i = 1; i <= 12; i++) {
            int numMethod = (int) (Math.random() * 5 + 1);
            fullBelly--;
            switch (numMethod) {
                case 1: {
                    if (reproduces() != true)
                        eat();
                }
                break;
                case 2: {
                    if (play() != true)
                        eat();
                }
                break;
                case 3: {
                    if (sleep() != true)
                        eat();
                }
                break;
                case 4: {
                    if (chaseMouse() != true)
                        eat();
                }
                break;
                case 5:
                    eat();
                    break;
                default:
                    break;

            }
        }
    }


    public void eat(int fullBelly) {
        this.fullBelly = +fullBelly;
        System.out.println("Котик хочет есть" + " " + foodName + " " + fullBelly);
    }

    public void eat(int fullBelly, String foodName) {
        this.fullBelly = +fullBelly;
        System.out.println("Котик хочет есть" + " " + foodName + " " + "так как очень голоден" + " " + fullBelly);
    }

    public void eat() {
        eat(fullBelly, foodName);
    }

    public boolean play() {
        if (fullBelly > 0) {
            System.out.println("Котик играет" + " " + fullBelly);
            return true;
        } else {
            return false;
        }
    }

    public boolean sleep() {
        if (fullBelly > 0) {
            System.out.println("Котик спит" + " " + fullBelly);
            return true;
        } else {
            return false;
        }
    }

    public boolean chaseMouse() {
        if (fullBelly > 0) {
            System.out.println("Котик ищет мышь" + " " + fullBelly);
            return true;
        } else {
            return false;
        }
    }

    public boolean reproduces() {
        if (fullBelly > 0) {
            System.out.println("Котик разможается" + " " + fullBelly);
            return true;
        } else {
            return false;
        }
    }
}

