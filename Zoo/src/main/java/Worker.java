import animals.Animal;
import animals.Voice;
import food.Food;

public class Worker {

    /* Сделал 3 параметра, чтобы было видно название животного которое ест или не ест */
    public static void feed(Animal animal, Food food)
    {
        if (animal.eat(food))
        {
            System.out.println(animal.getName() + " с удовольствем съест " + food.foodName());
        }
        else
        {
            System.out.println(animal.getName() + " не будет есть " + food.foodName());
        }
    }

    protected static String getVoice(Voice animal){
        return animal.voice();
    }
}
