
import animals.*;
import food.Food;
import food.Grass;
import food.Meat;

import java.util.ArrayList;

public class Zoo {

    public static void main(String[] args) {

        Food meat = new Meat();
        Food grass = new Grass();

        Duck duck = new Duck("Скрудж макдак");
        Eagle eagle = new Eagle("Пустынный орёл");
        Shark shark = new Shark("Тигровая акула");
        Fish fish = new Fish("Платва");
        Moose moose = new Moose("Бурый лось");
        Wolf wolf = new Wolf("Серый волк");

        Worker worker = new Worker();

        System.out.println(worker.getVoice(duck));
        System.out.println(worker.getVoice(eagle));

        duck.saved();
        eagle.hunts(eagle.getName());
        shark.hunts(shark.getName());

        System.out.println(worker.getVoice(duck));
        System.out.println(worker.getVoice(eagle));
        /* fish выдаёт ошибку компиляции в worker.getVoice(fish)*/

        Animal[] animals = {duck, eagle, shark, fish, moose, wolf};

        for (Animal animal : animals) {
            worker.feed(animal, grass);
        }

        for (Animal animal : animals) {
            worker.feed(animal, meat);
        }

        ArrayList<Swim> lake = new ArrayList<Swim>();
        lake.add(duck);
        lake.add(shark);
        lake.add(fish);

        for (Swim animal : lake) {
            animal.swim(2);
        }
    }
}
