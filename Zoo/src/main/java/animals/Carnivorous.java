package animals;

import food.Food;

public abstract class Carnivorous extends Animal {

    public Carnivorous(String name) {
        super(name);
    }

    @Override
    public boolean eat(Food food){
        return food instanceof food.Meat;
    }

    public void hunts(String animal){
        System.out.println(animal + " охотится на травоядных");
    }
}
