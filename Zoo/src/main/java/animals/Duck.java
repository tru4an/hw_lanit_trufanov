package animals;

public class Duck extends Herbivore implements Swim, Run, Fly, Voice {


    public Duck(String name) {
        super(name);
    }

    public String voice() {
        return "Кря кхкпчых";
    }

    public void fly(int fly) {
        System.out.println("Утка летает на высоте " + fly + " метров");
    }

    public void run(int run) {
        System.out.println("Утка бегает по полю");
    }

    public void swim(int swim) {
        System.out.println("Утка палавает в пруду со скоростью " + swim + " км.ч");
    }
}
