package animals;

public class Eagle extends Carnivorous implements Fly, Voice{

    public Eagle(String name) {
        super(name);
    }

    public String voice(){
        return "Ааааааааааа ааааааа";
    }

    public void fly(int fly) {
        System.out.println("Орёл летает на высоте " + fly + " метров");
    }

}
