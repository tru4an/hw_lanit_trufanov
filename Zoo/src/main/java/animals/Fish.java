package animals;

public class Fish extends Herbivore implements Swim {

    public Fish(String name) {
        super(name);
    }

    public void swim(int swim) {
        System.out.println("Рыба плавает на глубине " + swim + " метра(ов)");
    }
}
