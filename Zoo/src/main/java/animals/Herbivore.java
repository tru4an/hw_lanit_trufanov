package animals;

import food.Food;

public abstract class Herbivore extends Animal {

    public Herbivore(String name) {
        super(name);
    }

    @Override
    public boolean eat(Food food) {
        return food instanceof food.Grass;
    }

    public void saved() {
        System.out.println("Травоядные животные спасаются от хищников");
    }
}