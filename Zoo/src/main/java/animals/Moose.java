package animals;

public class Moose extends Herbivore implements Run, Voice {

    public Moose(String name) {
        super(name);
    }

    public String voice(){
        return "муууу";
    }

    public void run(int run) {
        System.out.println("Лось бегает со скоростью " + run + " километров");
    }
}
