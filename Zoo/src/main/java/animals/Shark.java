package animals;

public class Shark extends Carnivorous implements Swim {

    public Shark(String name) {
        super(name);
    }

    public void swim(int swim) {
        System.out.println("Акула плавает на глубине " + swim + " метра(ов)");
    }

    public void hunts(String animal){
        System.out.println(animal + " охотится на млекопитающих");
    }
}
