package animals;

public class Wolf extends Carnivorous implements Run, Voice {

    public Wolf(String name) {
        super(name);
    }

    public String voice(){
        return "рррррр";
    }

    public void run(int run) {
        System.out.println("Волк бегает со скоростью " + run + " километров");
    }
}
