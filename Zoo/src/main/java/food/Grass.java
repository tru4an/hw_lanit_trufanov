package food;

public class Grass extends Food {

    public boolean grass() {
        return true;
    }

    @Override
    public String foodName() {
        return "траву";
    }

}
